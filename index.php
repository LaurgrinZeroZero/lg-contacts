<?php
require_once("Autoloader.php");
$renderer = new \Model\Renderer();

$componentList = $renderer->getComponents($renderer::CONTACTS_PAGE);
$baseJsPath =  $_SERVER['REQUEST_SCHEME'] . "://" . $_SERVER['SERVER_NAME']
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Contacts</title>
    <meta charset="UTF-8">
    <script type="text/javascript" src="<?= $baseJsPath . '/js/validator.js' ?>"></script>
</head>
<body>
<span id="notification"></span>
<?php foreach ($componentList as $component): ?>
    <?php require_once($component) ?>
<?php endforeach; ?>
</body>
<footer>
    <hr>
    Created by LG. Copy this, or not, I'm not a police officer.
</footer>
</html>