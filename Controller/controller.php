<?php

use Controller\ContactsController;

require_once($_SERVER['DOCUMENT_ROOT'] . "/Autoloader.php");

/* Vars */
$contacts  = [];
$pageCount = 1;
$page      = 1;
$pageSize  = 50;

/** @var \Controller\ContactsController $control */
$control = ContactsController::getInstance();

if (count($_GET) > 0) {
    $page = $control->getCurrentPageNumber($_GET);
}

if (isset($_POST['insert']) && $_POST['insert'] == 1) {
    try {
        $control->insertRecord($_POST);
    } catch (\Model\Exception\InvalidDataException $e) {
        echo "<div>" . $e->getMessage() . "</div>>";
    }
}

if (isset($_POST['update']) && $_POST['update'] == 1) {
    try {
        $control->updateRecord($_POST);
    } catch (\Model\Exception\InvalidDataException $e) {
        echo "<div>" . $e->getMessage() . "</div>>";
    }
}

if (isset($_POST['delete']) && $_POST['delete'] == 1) {
    try {
        $control->deleteRecord($_POST['id']);
    } catch (\Model\Exception\InvalidDataException $e) {
        echo "<div>" . $e->getMessage() . "</div>>";
    }
}

/* Check if a controller instance exists and run actions */
if ($control) {
    $contacts  = $control->getCurrentPageContents();
    $pageCount = $control->getPageCount();
}

?>