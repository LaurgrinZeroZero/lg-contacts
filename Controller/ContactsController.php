<?php

namespace Controller;
require_once($_SERVER['DOCUMENT_ROOT'] . "/Autoloader.php");

use Model\Exception\InvalidDataException;
use Model\Factory;

class ContactsController
{
    /** @var int */
    protected $page;

    /** @var int */
    protected $pageSize;

    /** @var int */
    protected $pageCount;

    /** @var \Model\Validator */
    protected $validator;

    /** @var \Model\Contact\ContactRepository */
    protected $collection;

    /** @var string */
    protected $phoneSearchString;

    /** @var string */
    protected $nameSearchString;

    /**
     * ContactsController constructor.
     *
     * @param int $pageSize
     * @param int $page
     */
    public function __construct(int $pageSize = 50, int $page = 1)
    {
        $this->validator         = Factory::createValidator();
        $this->collection        = Factory::createContactCollection();
        $this->pageSize          = $pageSize;
        $this->page              = $page;
        $this->phoneSearchString = "";
        $this->nameSearchString  = "";
    }

    /**
     * Returns the contents of the current page from the Database.
     *
     * @return array
     */
    public function getCurrentPageContents(): array
    {
        return $this->collection->setConstraints($this->getConstraints())->fetch()->get();
    }

    /**
     * Return the constraints for the collection.
     *
     * @return array
     */
    protected function getConstraints(): array
    {
        return [
            "phoneNumber" => "%$this->phoneSearchString%",
            "name"        => "%$this->nameSearchString%",
            "limit"       => $this->pageSize,
            "offset"      => $this->getOffsetByPageNumber(),
        ];
    }

    /**
     * Gets page offset by the page size and current page.
     *
     * @param int $pageSize
     * @param int $page
     *
     * @return int
     */
    protected function getOffsetByPageNumber(): int
    {
        return $this->pageSize * ($this->page - 1);
    }

    /**
     * Returns a page count from the database.
     *
     * @return int
     */
    public function getPageCount(): int
    {
        if (!isset($this->pageCount)) {
            $this->pageCount = $this->collection->setConstraints($this->getConstraints())->getCount();
        }

        return $this->pageCount;
    }

    /**
     * Returns current page number
     *
     * @param array $params
     *
     * @return int
     */
    public function getCurrentPageNumber(array $params): int
    {
        $this->updateParams($params);

        return $this->page;
    }

    /**
     * Updates the params for the instance.
     *
     * @param array $params
     */
    protected function updateParams(array $params): void
    {
        if (!empty($params['page']) && $this->validator->validatePageNumber($params['page'], $this->getPageCount())) {
            $this->page = $params['page'];
        }

        if (!empty($params['phone'])) {
            $this->phoneSearchString = filter_var($params['phone'], FILTER_SANITIZE_STRING);
        }

        if (!empty($params['name'])) {
            $this->nameSearchString = filter_var($params['name'], FILTER_SANITIZE_STRING);
        }
    }

    /**
     * Inserts data into the database.
     *
     * @param array $data
     *
     * @throws \Model\Exception\InvalidDataException
     */
    public function insertRecord(array $data): void
    {
        $params = [
            "name"        => filter_var($data['name'], FILTER_SANITIZE_STRING),
            "phoneNumber" => filter_var($data['phone'], FILTER_SANITIZE_STRING),
        ];

        if ($this->validator->validateName($params['name']) && $this->validator->validateName($params['phoneNumber'])) {
            $this->collection->create($params);
        } else {
            throw new InvalidDataException("Data failed to validate while trying to insert a record. Data supplied: "
                                           . print_r($params, true));
        }
    }

    /**
     * Updates a database record.
     *
     * @param array $data
     *
     * @throws \Model\Exception\InvalidDataException
     */
    public function updateRecord(array $data):void
    {
        $params = [
            "name"        => filter_var($data['name'], FILTER_SANITIZE_STRING),
            "phoneNumber" => filter_var($data['phone'], FILTER_SANITIZE_STRING),
            "id"          => filter_var($data['id'], FILTER_SANITIZE_NUMBER_INT)
        ];

        if ($this->validator->validateContact($params)) {
            $this->collection->update($params);
        } else {
            throw new InvalidDataException("Data failed to validate while trying to update a record. Data supplied: "
                                           . print_r($params, true));
        }
    }

    /**
     * Asks the collection to remove the record with the id provided in the argument.
     *
     * @param int $id
     *
     * @throws \Model\Exception\InvalidDataException
     */
    public function deleteRecord(int $id):void
    {
        $id = filter_var($id, FILTER_SANITIZE_NUMBER_INT);

        if ($this->validator->validateId($id)) {
            $this->collection->delete($id);
        } else {
            throw new InvalidDataException("Id failed to validate while trying to delete a record. Id supplied: $id");
        }
    }
}