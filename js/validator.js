var notification = document.getElementById("notification");

/**
 * Validates page select form.
 *
 * @param pageCount
 * @returns {boolean}
 */
function validatePageForm(pageCount) {
    var form = document.pageForm;

    if (form.page.value === "") {
        notification.innerHTML = "Page number cannot be empty.";
        return false;
    }

    if (form.page.value <= 0) {
        notification.innerHTML = "Page number cannot be lower than 1.";
        return false;
    }

    if (form.page.value > pageCount) {
        notification.innerHTML = "Page number cannot higher than total amount of pages.";
        return false;
    }

    if (isNaN(form.page.value)) {
        notification.innerHTML = "Page number must be an actual number.";
        return false;
    }

    return true;
}

/**
 * Validates record insert form.
 *
 * @returns {boolean}
 */
function validateInsert() {
    var name = document.insertForm.name.value;
    var phone = document.insertForm.phone.value;

    if (name === "" || phone === "") {
        notification.innerHTML = "Form values cannot be empty.";
        return false;
    }

    return true;
}

/**
 * Validates record update
 *
 * @param id
 * @param name
 * @param phone
 * @returns {boolean}
 */
function validateUpdate(id, name, phone) {
    if (name === "" || phone === "" || id === "") {
        notification.innerHTML = "Form values cannot be empty.";
        return false;
    }

    if (id < 0) {
        notification.innerHTML = "Record ID cannot be negative. Stop editing the source, please.";
        return false;
    }

    if (isNaN(id)) {
        notification.innerHTML = "Record ID has to be an actual number. Stop editing the source, please.";
        return false;
    }

    return true;
}

/**
 * Validates record delete action
 *
 * @param id
 * @returns {boolean}
 */
function validateDelete(id) {
    if (id === "") {
        notification.innerHTML = "Form values cannot be empty.";
        return false;
    }

    if (id < 0) {
        notification.innerHTML = "Record ID cannot be negative. Stop editing the source, please.";
        return false;
    }

    if (isNaN(id)) {
        notification.innerHTML = "Record ID has to be an actual number. Stop editing the source, please.";
        return false;
    }

    return true;
}