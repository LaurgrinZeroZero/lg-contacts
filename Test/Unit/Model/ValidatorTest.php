<?php
namespace Test\Unit\Model;
require_once ("/home/sykas/domains/lg-test-dev.home.creation.lt/public_html/vendor/phpunit/phpunit/src/Framework/TestCase.php");
require_once ("/home/sykas/domains/lg-test-dev.home.creation.lt/public_html/Model/Validator.php");

use Model\Validator;
use PHPUnit\Framework\TestCase;

class ValidatorTest extends TestCase
{
    protected $validator;

    protected function setUp()
    {
        $this->validator = new Validator();
    }

    /**
     * @dataProvider validatePageNumberDataProvider
     * @param $page
     * @param $pageCount
     * @param $expected
     */
    public function testValidatePageNumber($page, $pageCount, $expected)
    {
        $result = $this->validator->validatePageNumber($page, $pageCount);
        $this->assertEquals($expected, $result);
    }

    /** @dataProvider validateIdDataProvider */
    public function testValidateId($id, $expected)
    {
        $result = $this->validator->validateId($id);
        $this->assertEquals($expected, $result);
    }

    /** @dataProvider validateNameDataProvider */
    public function testValidateName($string, $expected)
    {
        $result = $this->validator->validateName($string);
        $this->assertEquals($result, $expected);
    }

    /** @dataProvider validatePhoneNumberDataProvider */
    public function testValidatePhoneNumber($phone, $expected)
    {
        $result = $this->validator->validatePhoneNumber($phone);
        $this->assertEquals($result, $expected);
    }

    public function validatePageNumberDataProvider()
    {
        return [
            [1, 50, true],
            [0, 50, false],
            [-1, 50, false],
            [51, 50, false],
            ['a', 50, false],
            [null, 50, false],
            [1, -1, false],
            [1, 0, false],
            [null, null, false],
            [-100, -100, false],
            ["Hello", "world", false],
            ["", "", false],
            [$this->validator, $this->validator, false],
            [[-5, "bur"], 0, false],
        ];
    }

    public function validateIdDataProvider()
    {
        return [
          [1, true],
          [5, true],
          [201254, true],
          [0, false],
          [-1, false],
          [null, false],
          ["asdasd", false],
          [$this->validator, false],
          ["", false],
          [["foo", "bar"], false],
          [[], false],
        ];
    }

    public function validateNameDataProvider()
    {
        return [
            ["hello", true],
            [null, false],
            [$this->validator, false],
            [["foo", "bar"], false],
            [[], false],
            [-55165164, true],
            [0.2548, true],
        ];
    }

    public function validatePhoneNumberDataProvider()
    {
        return [
            ["+370 619 67796", true],
            ["8 619 67796", true],
            ["861967796", true],
            ["+37061967796", true],
            ["154684", false],
            ["asdas65d4as65d46", false],
            ["", false],
            [null, false],
            [$this->validator, false],
            [["+370 619 67796"], false],
        ];
    }
}
