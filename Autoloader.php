<?php

spl_autoload_extensions(".php");
spl_autoload_register(function($className) {
    $className = str_replace("\\", "/", $className);

    require_once $_SERVER['DOCUMENT_ROOT'] . "/$className.php";
});