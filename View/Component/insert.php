<form action="contacts.php" method="post" name="insertForm" onsubmit="return validateInsert()">
    <fieldset>
        <legend>Insert new record</legend>
        <div>
            <label for="name">Name</label>
            <input name="name" type="text" required>
        </div>
        <div>
            <label for="phone">Phone</label>
            <input name="phone" type="text" required>
        </div>
        <input type="hidden" name="insert" value="1">
        <input type="submit" value="Save">
    </fieldset>
</form>