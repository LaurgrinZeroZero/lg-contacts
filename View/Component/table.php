<?php if (!empty($contacts) && count($contacts) > 0): ?>
<table>
    <thead>
    <tr>
        <th>Name</th>
        <th>Phone</th>
    </tr>
    </thead>
    <tbody>
        <?php foreach ($contacts as $contact): ?>
            <tr>
                <form action="contacts.php" method="post"
                      onsubmit="return validateUpdate(<?= $contact->getId() . "," . $contact->getName() . "," . $contact->getPhoneNumber()?>)"
                >
                    <td><input type="text" name="name" value="<?= $contact->getName() ?>" required></td>
                    <td><input type="text" name="phone" value="<?= $contact->getPhoneNumber() ?>" required></td>
                    <td>
                        <input type="hidden" name="id" value="<?= $contact->getId() ?>">
                        <input type="hidden" name="update" value="1">
                        <input type="submit" value="Edit">
                    </td>
                </form>
                <td>
                    <form action="contacts.php" method="post" onsubmit="return validateDelete(<?= $contact->getId() ?>)">
                        <input type="hidden" name="delete" value="1">
                        <input type="hidden" name="id" value="<?= $contact->getId() ?>">
                        <input type="submit" value="Delete">
                    </form>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php else: ?>
<div>No results returned</div>
<?php endif; ?>
