<?php
if (!isset($page)) {
    echo "Page not set - reverting to default.<br>";
    $page = 1;
}

if (!isset($pageCount)) {
    echo "Page count not set - reverting to default.<br>";
    $pageCount = 50;
}
?>
<?php if ($pageCount > 0): ?>
    <?php if ($page > 1): ?>
        <form action="contacts.php" method="get">
            <input type="hidden" name="page" value="<?= $page - 1 ?>">
            <input type="submit" value="<">
        </form>
    <?php endif; ?>

    <form action="contacts.php" method="get" name="pageForm" onsubmit="return validatePageForm(<?= $pageCount ?>)">
        <input type="number" name="page" value="<?= $page ?>">
        <span> / <?= $pageCount ?></span>
        <input type="submit" value="Go">
    </form>

    <?php if ($page < $pageCount): ?>
        <form action="contacts.php" method="get">
            <input type="hidden" name="page" value="<?= $page + 1 ?>">
            <input type="submit" value=">">
        </form>
    <?php endif; ?>
<?php endif; ?>