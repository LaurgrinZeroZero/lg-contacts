<form name="searchForm" action="contacts.php" method="get" onsubmit="return validateSearch()">
    <fieldset>
        <legend>Search:</legend>
        <div>
            <label for="name">Name</label>
            <input name="name" type="text" value="<?= !empty($_GET['name']) ? $_GET['name'] : "" ?>">
        </div>
        <div>
            <label for="phone">Phone</label>
            <input name="phone" type="text" value="<?= !empty($_GET['phone']) ? $_GET['phone'] : "" ?>">
        </div>
        <input type="submit" value="Search">
    </fieldset>
</form>