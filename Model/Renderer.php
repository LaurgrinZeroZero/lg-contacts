<?php
namespace Model;

use Model\Exception\MissingComponentException;

class Renderer
{
    const CONTACTS_PAGE = 0;

    const COMPONENTS = [
        ["insert.php", "search.php", "table.php", "pagination.php"],
    ];

    /** @var string  */
    protected $componentDirectory;

    /** @var  */
    protected $viewDirectory;

    /**
     * Renderer constructor.
     */
    public function __construct()
    {
        $this->componentDirectory = $_SERVER['DOCUMENT_ROOT'] . "/View/Component/";
        $this->viewDirectory      = $_SERVER['DOCUMENT_ROOT'] . "/View/";
    }

    /**
     * Returns the components for the specified view.
     *
     * @param int $component
     *
     * @return array
     * @throws \Model\Exception\MissingComponentException
     */
    public function getComponents(int $component)
    {
        $components = [];

       foreach(self::COMPONENTS[$component] as $component) {
           $components[] = $this->componentDirectory . $component;
       }

        if (count($components) > 0) {
            return $components;
        } else {
            throw new MissingComponentException("No components found for the page.");
        }
    }
}