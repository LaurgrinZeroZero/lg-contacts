<?php

namespace Model;

use mysqli;

abstract class DatabaseHandler
{
    const TABLE_CONTACTS      = 'contact';
    const COLUMN_ID           = 'contact_id';
    const COLUMN_NAME         = 'contact_name';
    const COLUMN_PHONE_NUMBER = 'contact_tel';

    /** @var \mysqli */
    protected $connection;

    /**
     * DatabaseHandler constructor.
     *
     * @param string $user
     * @param string $databaseName
     * @param string $databasePassword
     * @param string $host
     */
    public function __construct(string $user, string $databaseName, string $databasePassword, string $host = "localhost")
    {
        mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
        $this->connection = new mysqli($host, $user, $databasePassword, $databaseName);

        if ($this->connection->connect_errno) {
            echo "Failed to connect to MySQL: (" . $this->connection->connect_errno . ") " . $this->connection->connect_error;
            exit("Connection to the database could not be established");
        }
    }

    /**
     * Closes the connection instance on destruction.
     */
    public function __destruct()
    {
        $this->connection->close();
    }

    /**
     * Checks if the correct model was received.
     *
     * @param $model
     *
     * @return bool
     */
    protected abstract function isModelValid($model): bool;
}