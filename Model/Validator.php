<?php

namespace Model;


class Validator
{
    /**
     * Validates page number
     *
     * @param     $page
     *
     * @param int $pageCount
     *
     * @return bool
     */
    public function validatePageNumber($page, $pageCount): bool
    {
        if (is_array($page) || is_array($pageCount)) {
            return false;
        }

        $page = trim($page);

        /* Check if page can be a number */
        if (!is_numeric($page)) {
            return false;
        }

        /* Check if page is not outside the bound */
        if ($page < 1 || $page > $pageCount) {
            return false;
        }

        return true;
    }

    /**
     * Validates a string
     *
     * @param string $string
     *
     * @return bool
     */
    public function validateName($string): bool
    {
        if (is_array($string)) {
            return false;
        }

        $string = trim($string);

        /* Check if string is empty */
        if (!isset($string) || $string == "") {
            return false;
        }

        return true;
    }

    /**
     * Validates a phone number
     *
     * @param $number
     *
     * @return bool
     */
    public function validatePhoneNumber($number)
    {
        $pattern = "/(\+\d{3}|8)\s*\d{3}\s*\d{5}/";

        if (is_array($number)) {
            return false;
        }

        if (!preg_match($pattern, $number)) {
            return false;
        }

        return true;
    }

    /**
     * Validates contact ID.
     *
     * @param int $id
     *
     * @return bool
     */
    public function validateId($id): bool
    {
        if (is_array($id)) {
            return false;
        }

        $id = trim($id);

        /* Check if id can be casted into a number */
        if (!is_numeric($id)) {
            return false;
        }

        /* Check if id has a legal value */
        if ($id < 1) {
            return false;
        }

        return true;
    }

    /**
     * Validates the contact model.
     *
     * @param $params
     *
     * @return bool
     */
    public function validateContact($params): bool
    {
        return $this->validateId($params["id"]) &&
               $this->validateName($params["name"]) &&
               $this->validateName($params["phoneNumber"]);
    }
}