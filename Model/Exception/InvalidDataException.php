<?php
namespace Model\Exception;

use Exception;
use Throwable;

class InvalidDataException extends Exception
{
    public function __construct($message = "", $code = 1, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public function __toString()
    {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}