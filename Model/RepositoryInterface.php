<?php

namespace Model;

interface RepositoryInterface
{
    /**
     * Returns the collections.
     *
     * @return array
     */
    public function read(): array;

    /**
     * Insert the model to the database
     *
     * @param $model
     *
     * @return bool
     */
    public function create($model): bool;

    /**
     * Updates the specified model.
     *
     * @param array $model
     *
     * @return bool
     */
    public function update($model): bool;

    /**
     * Deletes the specified model by ID.
     *
     * @param int $id
     *
     * @return bool
     */
    public function delete(int $id): bool;
}