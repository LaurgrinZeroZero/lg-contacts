<?php
namespace Model;

use Model\Contact\ContactRepository;

class Factory
{
    /**
     * Create and return a new instance of Validator
     *
     * @return \Model\Validator
     */
    public static function createValidator()
    {
        return new Validator();
    }

    /**
     * Create and return a new instance of Contact\ContactRepository
     */
    public static function createContactCollection()
    {
        return new ContactRepository();
    }
}