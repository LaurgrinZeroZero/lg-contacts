<?php

namespace Model\Contact;

class Contact
{
    /** @var string */
    protected $name;

    /** @var string */
    protected $phoneNumber;

    /** @var int */
    protected $id;

    /**
     * Contact constructor.
     *
     * @param string $name
     * @param string $phoneNumber
     * @param int    $id
     */
    public function __construct(string $name, string $phoneNumber, int $id = 0)
    {
        $this->setName($name);
        $this->setPhoneNumber($phoneNumber);
        $this->setId($id);
    }

    /**
     * Returns the contact's name.
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Returns the contact's phone number.
     *
     * @return string
     */
    public function getPhoneNumber(): string
    {
        return $this->phoneNumber;
    }

    /**
     * Returns the contact's ID
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @param string $phoneNumber
     */
    public function setPhoneNumber(string $phoneNumber): void
    {
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }
}