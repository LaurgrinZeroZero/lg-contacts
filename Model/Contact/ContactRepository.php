<?php

namespace Model\Contact;

use Model\Exception\InvalidModelException;
use Model\Exception\MissingConfigurationFileException;
use Model\RepositoryInterface;
use Model\DatabaseHandler;

class ContactRepository extends DatabaseHandler implements RepositoryInterface
{
    const MODEL_NAME = "\Model\Contact\Contact";

    /** @var array */
    protected $collection = [];

    /** @var array */
    protected $constraints = [];

    /**
     * ContactRepository constructor.
     *
     * @throws \Model\Exception\MissingConfigurationFileException
     */
    public function __construct()
    {
        $path = $_SERVER['DOCUMENT_ROOT'] . "/.env";

        if (!file_exists($path)) {
            throw new MissingConfigurationFileException("Cannot find .env in the root folder.", 0);
        }

        $config = json_decode(file_get_contents($path), true)['database'];

        parent::__construct($config["databaseUser"],  $config["databaseName"], $config["databasePassword"]);
    }

    /**
     * Unsets the reference to databaseHandler, closing the connection due to DatabaseHandler's destructor.
     */
    public function __destruct()
    {
        parent::__destruct();
    }

    /**
     * Returns the collection
     *
     * @return array
     */
    public function read(): array
    {
        return $this->collection;
    }

    /**
     * Sets the constraints for a query.
     *
     * @param array $constraints
     *
     * @return \Model\RepositoryInterface
     */
    public function setConstraints(array $constraints): RepositoryInterface
    {
        $this->constraints = $constraints;

        return $this;
    }

    /**
     * Returns the current constraints
     *
     * @return array
     */
    public function getConstraints(): array
    {
        return $this->constraints;
    }

    /**
     * Gets a collection from the database and load it to the class instance.
     *
     * @return mixed
     */
    protected function fetch(): RepositoryInterface
    {
        $results = [];

        $query = "SELECT * FROM " . self::TABLE_CONTACTS . " WHERE " . self::COLUMN_PHONE_NUMBER .
                 " LIKE ? AND " . self::COLUMN_NAME . " LIKE ? LIMIT ? OFFSET ?";

        $statement = $this->connection->prepare($query);
        $statement->bind_param("ssii", ...array_values($this->constraints));

        $statement->execute();
        $statement->bind_result($id, $phoneNumber, $name);

        while ($statement->fetch()) {
            $results[] = ["name" => $name, "phoneNumber" => $phoneNumber, "id" => $id];
        }
        $statement->close();

        foreach ($results as $result) {
            $this->collection[] = new Contact($result["name"], $result["phoneNumber"], (int)$result["id"]);
        }

        return $this;
    }

    /**
     * Insert the model to the database
     *
     * @param \Model\Contact\Contact $contact
     *
     * @return bool
     * @throws \Model\Exception\InvalidModelException
     */
    public function create($contact): bool
    {
        if (!$this->isModelValid($contact)) {
            throw new InvalidModelException(self::MODEL_NAME . " expected, " . get_class($contact) . " received.");
        }

        $query     = "INSERT INTO " . self::TABLE_CONTACTS . " (contact_tel, contact_name) VALUES (?, ?)";
        $statement = $this->connection->prepare($query);
        $statement->bind_param("ss", $contact->getPhoneNumber(), $contact->getName());
        $success = $statement->execute();
        $statement->close();

        return $success;
    }

    /**
     * Returns the amount of pages with the specified constraints.
     *
     * @return int
     */
    public function getCount(): int
    {
        $pageCount = 1;
        $query     = "SELECT COUNT(*) as entries FROM " . self::TABLE_CONTACTS .
                     " WHERE " . self::COLUMN_PHONE_NUMBER . " LIKE ? AND " . self::COLUMN_NAME . " LIKE ?";
        $statement = $this->connection->prepare($query);
        $statement->bind_param("ss", $this->constraints['phoneNumber'], $this->constraints['name']);

        $statement->execute();
        $statement->bind_result($rowCount);

        while ($statement->fetch()) {
            $pageCount = (int)ceil($rowCount / $this->constraints['limit']);
        }

        $statement->close();

        return $pageCount;
    }

    /**
     * Updates the specified model.
     *
     * @param \Model\Contact\Contact $contact
     *
     * @return bool
     * @throws \Model\Exception\InvalidModelException
     */
    public function update($contact): bool
    {
        if (!$this->isModelValid($contact)) {
            throw new InvalidModelException(self::MODEL_NAME . " expected, " . get_class($contact) . " received.");
        }

        $query = sprintf(
            "UPDATE %s SET %s = ?, %s = ? WHERE %s = ?",
            self::TABLE_CONTACTS,
            self::COLUMN_NAME,
            self::COLUMN_PHONE_NUMBER,
            self::COLUMN_ID
        );
        $statement = $this->connection->prepare($query);
        $statement->bind_param("ssi", $contact->getName(), $contact->getPhoneNumber(), $contact->getId());
        $success = $statement->execute();
        $statement->close();

        return $success;
    }

    /**
     * Deletes the specified model by ID.
     *
     * @param int $id
     *
     * @return bool
     */
    public function delete(int $id): bool
    {
        $query = sprintf("DELETE FROM %s WHERE %s = ?", self::TABLE_CONTACTS, self::COLUMN_ID);
        $statement = $this->connection->prepare($query);
        $statement->bind_param("i", $id);
        $success = $statement->execute();
        $statement->close();

        return $success;
    }

    /**
     * Checks if the correct model was received.
     *
     * @param $model
     *
     * @return bool
     */
    protected function isModelValid($model): bool
    {
        return get_class($model) == self::MODEL_NAME;
    }
}